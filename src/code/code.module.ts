import { CodeService } from './code.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import Code from './entity/code.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Code])],
    controllers: [],
    providers: [
        CodeService,
    ],
    exports: [CodeService]
  })
export class CodeModule {}
