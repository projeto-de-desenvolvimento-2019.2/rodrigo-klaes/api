import { Module } from '@nestjs/common';
import { StatusController } from './status.controller';
import Status  from './entity/status.entity';
import { StatusService } from './status.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Status])],
  controllers: [StatusController],
  providers: [StatusService]
})
export class StatusModule {}
