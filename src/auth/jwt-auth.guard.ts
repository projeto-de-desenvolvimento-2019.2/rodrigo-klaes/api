import { ExecutionContext, HttpException, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import permissionMapping from '../../permissionMapping'
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
    constructor(private jwtService: JwtService){
      super()
    }

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {          
        const [req] = context.getArgs();
        const role = req.headers.role
        const path = req.route.path 
        
        try {
          // Verifica se o usuário tem a permissão baseado na role e path
          // que ele está tentando acessar
          const permissions = permissionMapping[role][req.method.toLowerCase()][path.split('/')[1]] || []
          
          const isTrue = permissions.some( permission => permission === path)
          
          
          if(!isTrue){            
            throw new HttpException('Unauthorized', 401)
          }

          return super.canActivate(context);
        } catch (error) {
          console.error(error);          
          throw new HttpException('Unauthorized', 401)
        }
      }


      handleRequest(err, user, info) {   
        // You can throw an exception based on either "info" or "err" arguments
        if (err || !user) {
          throw err || new UnauthorizedException();
        }
        return user;
      }
    
}
