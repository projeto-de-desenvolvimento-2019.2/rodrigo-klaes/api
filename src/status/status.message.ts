export default {
  NOT_FOUND: {
      message: 'Status não encontrado',
      status: 404
  }
}