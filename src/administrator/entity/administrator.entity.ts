import BaseEntity from '../../base/base.entity';
import { Column, Entity } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity('administrators')
export default class Administrator extends BaseEntity {
  @Column()
  @ApiModelProperty()
  readonly name: string;

  @Column({unique: true})
  @ApiModelProperty()
  readonly email: string;
}