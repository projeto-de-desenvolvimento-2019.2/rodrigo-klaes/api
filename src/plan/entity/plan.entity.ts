import BaseEntity from '../../base/base.entity';
import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';
import User from 'src/user/entity/user.entity';
import Signature from 'src/signature/entity/signature.entity';

@Entity('plans')
export default class Plan extends BaseEntity {
  @Column()
  @ApiModelProperty()
  readonly name: string;

  @Column()
  @ApiModelProperty()
  readonly description: string;

  @Column()
  @ApiModelProperty()
  readonly price: number;

  
  @OneToMany(type => Signature, signature => signature.plan)
  readonly signatures: Signature[]

  @ManyToMany(type => User)
  @JoinTable({
        name: "signatures",
        joinColumn: {
            name: "plan_id",
            referencedColumnName: "id"
        },
        inverseJoinColumn: {
            name: "user_id",
            referencedColumnName: "id"
        }
    })
    readonly users: User[]
}
