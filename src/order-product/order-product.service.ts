import { Injectable, BadGatewayException, HttpException } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import OrderProduct from './entity/order-product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import messages from './order-product.message'
import { v4 as uuid } from 'uuid'
import { OrderService } from 'src/order/order.service';
import Order from 'src/order/entity/order.entity';
import { caculateTotalProductBought } from '../helpers/caculations.helper';
import { getAllParams } from 'src/base/type/getAllParams';

@Injectable()
export class OrderProductService extends BaseService<OrderProduct> {
    constructor(
        @InjectRepository(OrderProduct)
        private readonly orderProductRepository: Repository<OrderProduct>,
        private readonly orderService: OrderService
    ) {
        super(orderProductRepository, messages)
    }

  async getAll(params: getAllParams): Promise<ResponseType> {
    const { relations = [], query: { page = 1, size: take = 10, search, getAll} } = params;

    const skip = (page - 1) * take 

    const pagination = getAll ? {} : { skip, take }
    
    try {
      const [result, total] = await this.orderProductRepository.findAndCount({
        where: { is_active: true },
        relations,
        ...pagination
      });

      
      return {
        page: page,
        size: +take,
        total: +total,
        totalPage: Math.ceil(total/take),
        data: result || []
      } as any;
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

    async create(entity: OrderProduct): Promise<string> {
        try {
          const order = await this.orderService.get(entity.order_id)
          
          entity.id = uuid()
          const created = await this.orderProductRepository.save(entity);
          if(!created){
              throw new HttpException(messages.NOT_CREATED.message, messages.NOT_CREATED.status)
          }

          await this.updateOrderTotal(order, caculateTotalProductBought(entity.price, entity.amount))
          

          return created.id;
        } catch (error) {
          console.log(error);
          
          throw new BadGatewayException(error);
        }
      }

    private async updateOrderTotal(order: Order, price){
        const currentTotal = order.total
        const newTotal = currentTotal + price

        const result = await this.orderService.update(order.id, {total: newTotal})
        return result
    }

    async removeItem(id): Promise<string>{
      const orderProduct = await this.orderProductRepository.findOne(id)

      if(!orderProduct){
        throw new HttpException(messages.NOT_FOUND.message, messages.NOT_FOUND.status)
      }

      const order = await this.orderService.get(orderProduct.order_id)

      if(!order){
        throw new HttpException('Não foi possível encontrar o pedido.', messages.NOT_FOUND.status)
      }

      const total = order.total - (orderProduct.price * orderProduct.amount)

        
      await this.orderProductRepository.remove(orderProduct)
      
      await this.orderService.update(orderProduct.order_id, { ...order, total})
      

      return 'ok'
    }

    async updateItem(id: string, amount = 0): Promise<string>{
      
      if(!amount){
        return await this.removeItem(id)
      }

      const orderProduct = await this.orderProductRepository.findOne(id)
      if(!orderProduct){
        throw new HttpException(messages.NOT_FOUND.message, messages.NOT_FOUND.status)
      }

      const order = await this.orderService.get(orderProduct.order_id)

      if(!order){
        throw new HttpException('Não foi possível encontrar o pedido.', messages.NOT_FOUND.status)
      }

      const totalWithoutProductUpdating = order.total - (orderProduct.price * orderProduct.amount)
      const total = totalWithoutProductUpdating + (orderProduct.price * amount)

      await this.update(id, { ...orderProduct, amount })
      
      await this.orderService.update(orderProduct.order_id, { ...order, total})
      

      return 'ok'
    }
}
