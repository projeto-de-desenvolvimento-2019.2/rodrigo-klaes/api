import BaseEntity from '../../base/base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import Product from '../../product/entity/product.entity';

@Entity('categories')
export default class Category extends BaseEntity {
  @Column()
  @ApiModelPropertyOptional()
  readonly name: string;

  @OneToMany(type => Product, product => product.category)
  // @JoinColumn({name: 'id', referencedColumnName: 'category_id'})
  readonly products: Product[]
}