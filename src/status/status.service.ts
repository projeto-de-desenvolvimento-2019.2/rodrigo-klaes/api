
import { Injectable } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Status from './entity/status.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import messages from './status.message'

@Injectable()
export class StatusService extends BaseService<Status> {
    constructor(
        @InjectRepository(Status)
        private readonly statusRepository: Repository<Status>
    ) {
        super(statusRepository, messages)
    }
}
