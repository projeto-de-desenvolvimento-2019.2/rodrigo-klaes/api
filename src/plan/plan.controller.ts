import { Controller } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Plan from './entity/plan.entity';
import { PlanService } from './plan.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('plans')
@Controller('plans')
export class PlanController extends BaseController<Plan> {
  constructor(private readonly planService: PlanService){
    super(planService)
  }
}

