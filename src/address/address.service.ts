import { BadGatewayException, Injectable } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Address from './entity/address.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import messages from './address.message'

@Injectable()
export class AddressService extends BaseService<Address> {
    constructor(
        @InjectRepository(Address)
        private readonly addressRepository: Repository<Address>
    ) {
        super(addressRepository, messages)
    }

    async delete(id: string): Promise<boolean> {
        try {
            await this.addressRepository.delete(id)
            return true;
        } catch (error) {
            throw new BadGatewayException(error);
        }
    }
}
