import { Injectable, BadGatewayException, HttpException } from '@nestjs/common';
import BaseEntity from './base.entity';
import { IBaseService } from './IBase.service';
import { Like, Repository } from 'typeorm';
import { v4 as uuid } from 'uuid';
import { getAllParams } from './type/getAllParams';
@Injectable()
export class BaseService<T extends BaseEntity> implements IBaseService<T> {
  messages: any;
  constructor(
    private readonly genericRepository: Repository<T>,
    messages: any,
  ) {
    this.messages = messages;
  }

  async create(entity: any): Promise<string> {
    try {
      entity.id = uuid();      
      const created = await this.genericRepository.save(entity);
      return created.id;
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async getAll(params: getAllParams): Promise<ResponseType> {
    const { relations = [], query: { page = 1, size: take = 10, search, getAll} } = params;

    const skip = (page - 1) * take 

    const pagination = getAll ? {} : { skip, take }
    
    try {
      const [result, total] = await this.genericRepository.findAndCount({
        where: { is_active: true, name: Like(`%${search || ''}%`) },
        relations,
        ...pagination
      });

      
      return {
        page: page,
        size: +take,
        total: +total,
        totalPage: Math.ceil(total/take),
        data: result || []
      } as any;
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async get(id: string, relations = []): Promise<T> {
    try {
      const result = await this.genericRepository.findOne(id, { relations, where: { is_active: true} });
      if (!result) {
        throw new HttpException(
          this.messages.NOT_FOUND.message,
          this.messages.NOT_FOUND.status,
        );
      }
      return result;
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async delete(id: string): Promise<boolean> {
    try {
      await this.update(id, { is_active: false });
      return true;
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async update(id: string, entity: any): Promise<any> {
    try {
      const result = await this.genericRepository.update(id, { ...entity });
      const data = this.genericRepository.findOne(id);
      if (!data) {
        throw new HttpException(
          this.messages.NOT_FOUND.message,
          this.messages.NOT_FOUND.status,
        );
      }
      return data;
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }
}
