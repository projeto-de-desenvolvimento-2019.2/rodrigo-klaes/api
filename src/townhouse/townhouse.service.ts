import { Injectable } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Townhouse from './entity/townhouse.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import messages from './townhouse.message'

@Injectable()
export class TownhouseService extends BaseService<Townhouse> {
    constructor(
        @InjectRepository(Townhouse)
        private readonly townhouseRepository: Repository<Townhouse>
    ) {
        super(townhouseRepository, messages)
    }
}

