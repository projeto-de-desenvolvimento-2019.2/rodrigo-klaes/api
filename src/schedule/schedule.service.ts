import { BadGatewayException, Injectable } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Schedule from './entity/schedule.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import messages from './schedule.message'
import { getAllParams } from 'src/base/type/getAllParams';
import * as XLSX from 'xlsx'

@Injectable()
export class ScheduleService extends BaseService<Schedule> {
    constructor(
        @InjectRepository(Schedule)
        private readonly scheduleRepository: Repository<Schedule>
    ) {
        super(scheduleRepository, messages)
    }

    async getNextDeliveryByUser(user_id: string, relations: Array<string> = []): Promise<Schedule>{
        return await this.scheduleRepository.findOne({ relations, where: { user_id, status_id: 2 }, order: { date: "ASC"}})
    }

    async getLastDeliveredByUser(user_id: string, relations: Array<string> = []): Promise<Schedule>{
        return await this.scheduleRepository.findOne({ relations, where: { user_id, status_id: 4 }, order: { updated_at: "ASC"}})
    }

    async getAll(params: getAllParams): Promise<ResponseType> {
        const { relations = [], query: { page = 1, size: take = 10, search, getAll} } = params;
    
        const skip = (page - 1) * take 
    
        const pagination = getAll ? {} : { skip, take }
        
        try {
          const [result, total] = await this.scheduleRepository.findAndCount({
            where: { is_active: true },
            order: { date: 'DESC'},
            relations,
            ...pagination
          });
    
          
          return {
            page: page,
            size: +take,
            total: +total,
            totalPage: Math.ceil(total/take),
            data: result || []
          } as any;
        } catch (error) {
          throw new BadGatewayException(error);
        }
      }

    async nextDeliveries(params: getAllParams): Promise<ResponseType> {
        const { relations = [], query: { page = 1, size: take = 10, search, getAll} } = params;
    
        const skip = (page - 1) * take 
    
        const pagination = getAll ? {} : { skip, take }
        
        try {
          const [result, total] = await this.scheduleRepository.findAndCount({
            where: { is_active: true, status_id: 2 },
            order: { date: 'DESC'},
            relations,
            ...pagination
          });
    
          
          return {
            page: page,
            size: +take,
            total: +total,
            totalPage: Math.ceil(total/take),
            data: result || []
          } as any;
        } catch (error) {
          throw new BadGatewayException(error);
        }
      }


    async exportNextDeliveries(): Promise<any>{
        
      const infos = await this.scheduleRepository.find({
        where: { is_active: true, status_id: 2 },
        order: { date: 'DESC'},
        relations: ['order', 'status', 'user', 'user.townhouses']
      })

      

      const data = infos.map(info => ({
        'Data da entrega': info.date,
        Endereço: info.user.townhouses[0].address + ', ' + info.user.townhouses[0].number,
        Cliente: info.user.name,
        'Código': info.order.code,
        Status: info.status.name
      }))
      

      const sheet = XLSX.utils.json_to_sheet(data)
      const wb = XLSX.utils.book_new()
      XLSX.utils.book_append_sheet(wb, sheet, 'fileName')
      
      const filename = `temp/uploads/next-deliveries.xlsx`

      await XLSX.writeFile(wb, filename)

      return filename
  }
}
