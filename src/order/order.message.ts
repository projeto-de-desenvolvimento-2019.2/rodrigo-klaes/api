export default {
    NOT_FOUND: {
        message: 'Ordem não encontrada',
        status: 404
    }
}