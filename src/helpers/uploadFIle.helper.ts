import { v2 as cloudinary} from 'cloudinary'



const uploadImage = async (path: string): Promise<string> => {    
    cloudinary.config({ 
      cloud_name: process.env.CLOUD_NAME, 
      api_key: process.env.CLOUD_API_KEY, 
      api_secret: process.env.CLOUD_API_SECRET
    });
    try {
        const result = await cloudinary.uploader.upload(
            path
        );
        
        return result.url
    } catch (error) {
        console.log('Problema ao fazer upload da imagem')
        throw new Error('Problema ao fazer upload da imagem')
    }
}

export default uploadImage