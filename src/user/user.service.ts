import { Injectable, HttpException, BadGatewayException } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import User from './entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager } from 'typeorm';
import messages from './user.message'
import UserQuerys from './types/user.querys';
import UserSignatureResponse from './types/userSignature.response';
import {ScheduleService} from '../schedule/schedule.service'
import Schedule from 'src/schedule/entity/schedule.entity';
import { FeedbackService } from 'src/feedback/feedback.service';
import * as XLSX from 'xlsx'
@Injectable()
export class UserService extends BaseService<User> {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly scheduleService: ScheduleService,
        private readonly feedbackService: FeedbackService
    ) {
        super(userRepository, messages);
    }

    async getUserByEmail(email: string): Promise<User>{
        const user = await this.userRepository.findOne({email});
        if(!user){
            throw new HttpException('Email ou Senha inválidos.', 401);
        }
        return user
    }

    async getOrders(id: string, query?: UserQuerys): Promise<User>{
        const qb = getManager()
                        .createQueryBuilder(User, 'users')
                        .innerJoinAndSelect('users.orders', 'orders');

        
        if(JSON.stringify(query) !== '{}'){   
            qb.where('orders.status = :status', {status: query.status});
        }
        qb.andWhere('users.id = :id', { id: id });
        qb.orderBy('orders.created_at', 'DESC')
        
        // console.log(qb.printSql());
        
        const user = await qb.getOne();

        if(!user){
            throw new HttpException(messages.NOT_FOUND.message, messages.NOT_FOUND.status);
        }

        return user;
    }

    async getSignatures(id: string, relations = []): Promise<UserSignatureResponse[]>{
        try {
            const result = await this.userRepository.findOne(id, { relations, where: { is_active: true} });

            if (!result) {
              throw new HttpException(
                this.messages.NOT_FOUND.message,
                this.messages.NOT_FOUND.status,
              );
            }

            const response = result.signatures.map(
                signature => (
                    {
                        user_id: result.id,
                        username: result.name,
                        plan_name: signature.plan.name,
                        price: signature.price,
                        plan_id: signature.plan.id,
                        plan_description: signature.plan.description,
                        id: signature.id,
                    }
                )
            )
            

            return response;
          } catch (error) {
            throw new BadGatewayException(error);
          }
    }

    async checkUser(id): Promise<User>{
        try {
            const data = this.userRepository.findOne(id);
            if (!data) {
                throw new HttpException(
                this.messages.NOT_FOUND.message,
                this.messages.NOT_FOUND.status,
                );
            }
            await this.userRepository.update(id, { is_checked: true });

            return data;
        } catch (error) {
            throw new BadGatewayException(error);
        }
    }

    async me(user_id: string): Promise<any> {
        
        const next = await this.scheduleService.getNextDeliveryByUser(user_id, ['order', 'status'])
        const last = await this.scheduleService.getLastDeliveredByUser(user_id, ['order', 'status'])
        let feedback;

        if(last?.order_id){
            feedback = await this.feedbackService.findByOrder(last.order_id)       
        }
         

        return {
            next: next ? await this.formatOrderMe(next) : {},
            last: last ? await this.formatOrderMe(last) : {},
            hasFeedback: feedback || !last ? true : false
        }
    }

    async formatOrderMe(data: Schedule): Promise<any>{
        return {
            ...data.order,
            status: data.status.name,
            status_id: data.status.id,
            date: data.date
        }
    }

    async export(): Promise<any>{
        
        const infos = await this.userRepository.find()

        const resp = infos.map(info => ({
            Id: info.id,
            'Ativo': info.is_active ? 'Sim' : 'Não',
            'Data da criação': info.created_at,
            Nome: info.name,
            Email: info.email,
            Verificado: info.is_checked ? 'Sim' : 'Não',
            Tipo: info.role,
        }))

        const sheet = XLSX.utils.json_to_sheet(resp)
        const wb = XLSX.utils.book_new()
        XLSX.utils.book_append_sheet(wb, sheet, 'fileName')
        
        const filename = `temp/uploads/users.xlsx`

        await XLSX.writeFile(wb, filename)

        return filename
    }
}
