import { Controller } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Address from './entity/address.entity';
import { AddressService } from './address.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('addresses')
@Controller('addresses')
export class AddressController extends BaseController<Address> {
    constructor(private readonly addressService: AddressService){
        super(addressService)
    }
}
