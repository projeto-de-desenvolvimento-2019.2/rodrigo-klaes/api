import { Controller } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Feedback from './entity/feedback.entity';
import { FeedbackService } from './feedback.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('feedbacks')
@Controller('feedbacks')
export class FeedbackController extends BaseController<Feedback> {
  constructor(private readonly feedbackService: FeedbackService) {
    super(feedbackService);
  }
}
