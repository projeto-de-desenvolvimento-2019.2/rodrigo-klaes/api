import { BadGatewayException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/base/base.service';
import { getAllParams } from 'src/base/type/getAllParams';
import { Like, Repository } from 'typeorm';
import Feedback from './entity/feedback.entity'
import messages from './feedback.message'
@Injectable()
export class FeedbackService extends BaseService<Feedback> {
  constructor(
    @InjectRepository(Feedback)
    private readonly feedbackRepository: Repository<Feedback>
  ) {
      super(feedbackRepository, messages)
  }

  async getAll(params: getAllParams): Promise<ResponseType> {
    const { relations = [], query: { page, size: take = 10, search, getAll} } = params;

    const skip = (page - 1) * take 

    const pagination = getAll ? { skip, take } : {}

    try {
      const [result, total] = await this.feedbackRepository.findAndCount({
        where: { is_active: true, comment: Like(`%${search || ''}%`) },
        relations,
        ...pagination
      });
      return {
        page: page,
        size: take,
        total: total,
        totalPage: Math.ceil(total/take),
        data: result || []
      } as any;
    } catch (error) {
      throw new BadGatewayException(error);
    }
  }

  async findByOrder(order_id: string): Promise<Feedback>{
    return await this.feedbackRepository.findOne({ where: { order_id } })
  }
}
