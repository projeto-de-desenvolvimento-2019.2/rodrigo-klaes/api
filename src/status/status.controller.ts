import { Controller } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Status from './entity/status.entity';
import { StatusService } from './status.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('status')
@Controller('status')
export class StatusController extends BaseController<Status> {
    constructor(private readonly statusService: StatusService){
        super(statusService)
    }
}

