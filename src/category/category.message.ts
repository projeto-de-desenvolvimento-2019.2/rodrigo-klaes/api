export default {
    NOT_FOUND: {
        message: 'Categoria não encontrada',
        status: 404
    }
}