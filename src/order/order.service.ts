import { Injectable, HttpException, BadGatewayException } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Order from './entity/order.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import messages from './order.message'
import { getAllParams } from 'src/base/type/getAllParams';
import generateRandomCode from 'src/helpers/generateCode';
import uuid = require('uuid');

@Injectable()
export class OrderService extends BaseService<Order> {
    constructor(
        @InjectRepository(Order)
        private readonly orderRepository: Repository<Order>
    ) {
        super(orderRepository, messages)
    }

    async getAll(params: getAllParams): Promise<ResponseType> {
      const { relations = [], query: { page, size: take = 10, search, getAll} } = params;

      const skip = (page - 1) * take 

      const pagination = getAll ? { skip, take } : {}

      try {
        const [result, total] = await this.orderRepository.findAndCount({
          where: { is_active: true, code: Like(`%${search || ''}%`) },
          relations,
          ...pagination
        });
        return {
          page: page,
          size: take,
          total: total,
          totalPage: Math.ceil(total/take),
          data: result || []
        } as any;
      } catch (error) {
        throw new BadGatewayException(error);
      }
    }


    async create(entity: Order): Promise<string> {
      try {
        entity.id = uuid();      
        const code = `BR${generateRandomCode(8)}${new Date().getFullYear()}`;      
        const created = await this.orderRepository.save({ ...entity, code });
        return created.id;
      } catch (error) {
        throw new BadGatewayException(error);
      }
    }

    async get(id: string, relations = []): Promise<Order> {
        try {
          const { orderProduct, ...order} = await this.orderRepository.findOne(id, {relations});
          
          if(orderProduct){
            order.products = orderProduct.map(item => {
                return {
                    id: item.product.id,
                    name: item.product.name,
                    code: item.product.code,
                    amount: item.amount,
                    price: item.price,
                    image: item.product.image,
                    unity: item.product.unity,
                    order_product_id: item.id
                }
            }) as any[];
          }
          
          if(!order){
            throw new HttpException(this.messages.NOT_FOUND.message, this.messages.NOT_FOUND.status);
          }
          return order as any;
        } catch (error) {
          throw new BadGatewayException(error);
        }
      }
}
