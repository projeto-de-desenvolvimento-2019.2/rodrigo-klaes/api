import { BadGatewayException, Injectable } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Signature from './entity/signature.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import messages from './signature.message'
import { getAllParams } from 'src/base/type/getAllParams';

@Injectable()
export class SignatureService extends BaseService<Signature> {
    constructor(
        @InjectRepository(Signature)
        private readonly signatureRepository: Repository<Signature>
    ) {
        super(signatureRepository, messages)
    }

    async getAll(params: getAllParams): Promise<ResponseType> {
        const { relations = [], query: { page = 1, size: take = 10, search, getAll} } = params;
    
        const skip = (page - 1) * take 
    
        const pagination = getAll ? {} : { skip, take }
        
        try {
          const [result, total] = await this.signatureRepository.findAndCount({
            where: { is_active: true },
            relations,
            ...pagination
          });
    
          
          return {
            page: page,
            size: +take,
            total: +total,
            totalPage: Math.ceil(total/take),
            data: result || []
          } as any;
        } catch (error) {
          throw new BadGatewayException(error);
        }
      }

      async delete(id: string): Promise<boolean> {
          try {
              await this.signatureRepository.delete(id)
              return true;
          } catch (error) {
              throw new BadGatewayException(error);
          }
      }
}

