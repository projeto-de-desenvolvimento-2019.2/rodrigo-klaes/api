import { FeedbackModule } from './feedback/feedback.module';
import { CodeModule } from './code/code.module';
import { MailModule } from './mail/mail.module';

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryModule } from './category/category.module';
import { Connection } from 'typeorm';
import { AppController } from './app.controller'
import { ProductModule } from './product/product.module';
import { UserModule } from './user/user.module';
import { OrderModule } from './order/order.module';
import { OrderProductModule } from './order-product/order-product.module';
import { ScheduleModule } from './schedule/schedule.module';
import { ConfigModule } from '@nestjs/config';
import { StatusModule } from './status/status.module';
import { PlanModule } from './plan/plan.module';
import { AdministratorModule } from './administrator/administrator.module';
import { SignatureModule } from './signature/signature.module';
import { TownhouseModule } from './townhouse/townhouse.module';
import { AddressModule } from './address/address.module';
import { AuthModule } from './auth/auth.module';


@Module({
  imports: [
        FeedbackModule, 
        CodeModule, 
    MailModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      keepConnectionAlive: false,
    }),
    CategoryModule,
    ProductModule,
    UserModule,
    OrderModule,
    OrderProductModule,
    ScheduleModule,
    StatusModule,
    PlanModule,
    AdministratorModule,
    SignatureModule,
    TownhouseModule,
    AddressModule,
    AuthModule,
  ],
  controllers: [AppController],
})
export class AppModule {
  constructor(private readonly connection: Connection) { }
}
