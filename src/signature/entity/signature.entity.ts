import BaseEntity from '../../base/base.entity';
import { Column, Entity, ManyToOne, JoinColumn } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';
import User from 'src/user/entity/user.entity';
import Plan from 'src/plan/entity/plan.entity';

@Entity('signatures')
export default class Signature extends BaseEntity {
  @Column({type: "double"})
  @ApiModelProperty()
  readonly price: number;

  @Column()
  @ApiModelProperty()
  readonly user_id: string

  @Column()
  @ApiModelProperty()
  readonly plan_id: string

  @ManyToOne(type => Plan)
  @JoinColumn({name: 'plan_id'})
  readonly plan: Plan

  
  @ManyToOne(type => User, user => user.signatures)
  @JoinColumn({name: 'user_id'})
  readonly user: User
}