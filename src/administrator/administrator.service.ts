import { Injectable } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Administrator from './entity/administrator.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import messages from './administrator.message'

@Injectable()
export class AdministratorService extends BaseService<Administrator> {
    constructor(
        @InjectRepository(Administrator)
        private readonly administratorRepository: Repository<Administrator>
    ) {
        super(administratorRepository, messages)
    }
}
