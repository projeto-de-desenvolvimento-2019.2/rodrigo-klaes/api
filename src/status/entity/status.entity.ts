import { Entity, Column } from "typeorm";
import BaseEntity from "../../base/base.entity";
import { ApiModelProperty } from "@nestjs/swagger";

@Entity('status')
export default class Status extends BaseEntity {
    @Column()
    @ApiModelProperty()
    readonly name: string

    @Column()
    @ApiModelProperty({nullable: true})
    readonly descrition: string
}