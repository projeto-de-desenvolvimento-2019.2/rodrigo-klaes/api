import { Injectable, HttpException } from '@nestjs/common';
import User from 'src/user/entity/user.entity';
import { UserService } from '../user/user.service';
import AuthInput from './types/auth.input';
import { JwtService } from '@nestjs/jwt';
import AuthOutput from './types/auth.output';
import * as bcrypt from 'bcrypt'
import MailService from 'src/mail/mail.service';
import { CodeService } from 'src/code/code.service';
import { NewPasswordParams, VerifyCodeParams } from 'src/code/type/code.type';
@Injectable()
export class AuthService {
  constructor(
      private usersService: UserService,
      private jwtService: JwtService,
      private  mailService: MailService,
      private  codeService: CodeService
    ) {}
  async validade(body: AuthInput): Promise<User>{
      const user = await this.usersService.getUserByEmail(body.email)

      const isPasswordMatching = await bcrypt.compare(body.pass, user.password);

      if(!isPasswordMatching){
          throw new HttpException('Email ou Senha inválida', 401)
      }
      return user
  }

  async login(user: User): Promise<AuthOutput> {
      const payload = { email: user.email, sub: user.id, role: user.role };
      return {
        access_token: this.jwtService.sign(payload),
        user_id: user.id,
        role: user.role
      };
  }

  async signIn(body: User): Promise<string>{
    const hash = await bcrypt.hash(body.password, 10);
    const created = await this.usersService.create({...body, password: hash})
    const code = await this.codeService.generate(created)

    try {
      await this.mailService.sendEmail({
          to: body.email, // list of receivers
          from: 'padocadeliveryapp@gmail.com', // sender address
          subject: 'Código de Verificação', // Subject line
          text: 'Código de verificação', // plaintext body
          html: `
          <h1>Código de verificação</h1>
          <p>${code}<p>
          `, // HTML body content
      })
    } catch (error) {
      console.log(error);
    }
    return created
  }

  async newPassword(data: NewPasswordParams): Promise<string>{
    const hash = await bcrypt.hash(data.password, 10);
    
    const updated = await this.usersService.update(data.user_id,{ password: hash})

    return updated
  }

  async reset(email: string): Promise<string>{
    const user = await this.usersService.getUserByEmail(email)
    const code = await this.codeService.generate(user.id, 8)
    try {
      await this.mailService.sendEmail({
          to: email, // list of receivers
          from: 'padocadeliveryapp@gmail.com', // sender address
          subject: 'Nova de senha', // Subject line
          text: 'Nova de senha', // plaintext body
          html: `
          <h1>Código de confirmação</h1>
          <p>${code}<p>
          `, // HTML body content
      })
    } catch (error) {
      console.log(error);
    }
    return user.id
  }

  async confirmCode(data: VerifyCodeParams): Promise<boolean>{
    const code = await this.codeService.verifyCode(data)
    await this.usersService.checkUser(code.user_id)
    await this.codeService.deleteCode(code.id)

    return true
  }
}


