# Padoca API
## Introdução
API desenvolvido em Nodejs, utilizando o framework NestJs e Typescript. 

## Documentação

https://documenter.getpostman.com/view/4059299/SWDzeg1Y?version=latest

## Requisitos

- [NodeJs](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/) (Obs: É instalado junto com o NodeJs)
- [NestJs](https://docs.nestjs.com/)

## Instalação

```bash
$ npm install
```

## Rodando o app

- Primeiro setar as variáveis de ambiente
- Em seguida, executar um dos comandos abaixo, conforme o ambiente.

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```


- Autor - [Rodrigo Klaes](https://www.linkedin.com/in/klaesrodrigo/)


