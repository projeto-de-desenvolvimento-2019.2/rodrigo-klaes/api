import { Entity, Column } from "typeorm";
import BaseEntity from "../../base/base.entity";
import { ApiModelProperty } from "@nestjs/swagger";

@Entity('codes')
export default class Code extends BaseEntity {
    @Column()
    @ApiModelProperty()
    readonly code: string

    @Column()
    @ApiModelProperty()
    readonly user_id: string
}