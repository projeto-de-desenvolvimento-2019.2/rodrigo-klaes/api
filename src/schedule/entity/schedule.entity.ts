import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";
import BaseEntity from "../../base/base.entity";
import { ApiModelProperty } from "@nestjs/swagger";
import User from "src/user/entity/user.entity";
import Order from "src/order/entity/order.entity";
import Status from "src/status/entity/status.entity";

@Entity('schedules')
export default class Schedule extends BaseEntity {
    @Column()
    @ApiModelProperty()
    readonly user_id: string

    @Column()
    @ApiModelProperty()
    readonly order_id: string

    @Column()
    @ApiModelProperty()
    readonly status_id: string

    @Column({type: "datetime"})
    @ApiModelProperty()
    readonly date: number

    @ManyToOne(type => Order)
    @JoinColumn({name: 'order_id'})
    readonly order: Order

    @ManyToOne(type => User)
    @JoinColumn({name: 'user_id'})
    readonly user: User

    @ManyToOne(type => Status)
    @JoinColumn({name: 'status_id'})
    readonly status: Status
}
