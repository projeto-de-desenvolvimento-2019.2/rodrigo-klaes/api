import { Controller, Post, UseGuards, Request, UseInterceptors, Body } from '@nestjs/common';
import AuthOutput from './types/auth.output';
import { AuthService } from './auth.service';
import { ApiResponse } from '@nestjs/swagger';
import { LocalAuthGuard } from './local-auth.guard';
import User from 'src/user/entity/user.entity';
import { NewPasswordParams, VerifyCodeParams } from 'src/code/type/code.type';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService){}

    @UseGuards(LocalAuthGuard)
    @Post('/login')
	@ApiResponse({ status: 400, description: 'Bad Request.'})
    async login(@Request() req: any): Promise<AuthOutput>{
        const authOutput = await this.authService.login(req.user) 
        
        return authOutput
    }

    @Post('/signin')
	@ApiResponse({ status: 400, description: 'Bad Request.'})
    async signIn(@Body() body: User): Promise<string>{        
        const signIn = await this.authService.signIn(body) 
        return signIn
    }

    @Post('/reset')
	@ApiResponse({ status: 400, description: 'Bad Request.'})
    async reset(@Body('email') email: string): Promise<string>{        
        const user = await this.authService.reset(email) 
        return user
    }

    @Post('/reset/password')
	@ApiResponse({ status: 400, description: 'Bad Request.'})
    async newPassword(@Body() body: NewPasswordParams): Promise<string>{        
        const user = await this.authService.newPassword(body) 
        return user
    }

    @Post('/confirm')
	@ApiResponse({ status: 400, description: 'Bad Request.'})
    async confirmCode(@Body() body: VerifyCodeParams): Promise<boolean>{        
        const isTrue = await this.authService.confirmCode(body)
        return isTrue
    }
}
