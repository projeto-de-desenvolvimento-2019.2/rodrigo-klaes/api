export default {
    NOT_FOUND: {
        message: 'Produto não encontrado',
        status: 404
    }
}