export default {
    NOT_FOUND: {
        message: 'Usuário não encontrado',
        status: 404
    }
}