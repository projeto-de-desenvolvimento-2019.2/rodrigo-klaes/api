import BaseEntity from '../../base/base.entity';
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import User from 'src/user/entity/user.entity';

@Entity('townhouses')
export default class Townhouse extends BaseEntity {
  @Column()
  @ApiModelProperty()
  readonly name: string;

  @Column()
  @ApiModelProperty()
  readonly address: string;

  @Column()
  @ApiModelProperty()
  readonly number: string;

  @Column()
  @ApiModelPropertyOptional()
  readonly complement: string;

  @Column()
  @ApiModelProperty()
  readonly district: string;

  @Column()
  @ApiModelProperty()
  readonly city: string;

  @Column()
  @ApiModelProperty()
  readonly state: string;

  @Column()
  @ApiModelProperty()
  readonly country: string;

  @Column()
  @ApiModelProperty()
  readonly zipcode: string;

  @ManyToMany(type => User)
  @JoinTable({
        name: "address",
        joinColumn: {
          name: "townhouse_id",
          referencedColumnName: "id"            
        },
        inverseJoinColumn: {
          name: "user_id",
          referencedColumnName: "id"
        }
    })
    readonly townhouses: Townhouse[]
}