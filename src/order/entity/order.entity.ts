import { Entity, Column, ManyToMany, JoinTable, ManyToOne, JoinColumn, OneToMany, OneToOne } from "typeorm";
import BaseEntity from "../../base/base.entity";
import { ApiModelProperty } from "@nestjs/swagger";
import Product from "src/product/entity/product.entity";
import User from "src/user/entity/user.entity";
import OrderProduct from "src/order-product/entity/order-product.entity";
import generateRandomCode from "src/helpers/generateCode";
import Schedule from "src/schedule/entity/schedule.entity";

export enum OrderStatus {
    PENDENTE = 'pendente',
    ENTREGUE = 'finalizado',
    CANCELADO = 'cancelado'
}

@Entity('orders')
export default class Order extends BaseEntity {
    @Column()
    @ApiModelProperty()
    readonly code: string

    @Column()
    @ApiModelProperty()
    readonly user_id: string

    @Column({
        type: "enum",
        enum: OrderStatus,
      })
    @ApiModelProperty()
    readonly status: OrderStatus

    @Column({type: "float"})
    @ApiModelProperty()
    readonly total: number

    @ManyToMany(type => Product)
    @JoinTable({
        name: "order_products",
        joinColumn: {
            name: "order_id",
            referencedColumnName: "id"
        },
        inverseJoinColumn: {
            name: "product_id",
            referencedColumnName: "id"
        }
    })
    readonly products: Product[]

    
    @ManyToOne(type => User, user => user.orders)
    @JoinColumn({name: 'user_id'})
    readonly user: User

    @OneToMany(type => OrderProduct, orderProduct => orderProduct.order)
    readonly orderProduct: OrderProduct[]

    @OneToOne(type => Schedule, schedule => schedule.order)
    readonly schedule: Schedule
}