import { Controller, Get, Param, Query, Req, Res, UseGuards } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import User from './entity/user.entity';
import { UserService } from './user.service';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import UserQuerys from './types/user.querys';
import UserSignatureResponse from './types/userSignature.response';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
@ApiUseTags('users')
@Controller('users')
export class UserController extends BaseController<User> {
  constructor(private readonly userService: UserService){
      super(userService)
  }
  
  @Get(':id')
  @ApiResponse({ status: 200, description: 'Entity retrieved successfully.' })
  @ApiResponse({ status: 404, description: 'Entity does not exist' })
  @UseGuards(JwtAuthGuard)
  async findById(@Param('id') id: string): Promise<User> {
    return await this.userService.get(id, ['signatures', 'signatures.plan', 'addresses', 'addresses.townhouse', 'townhouses']);
  }  

  @Get('/:id/orders')
	@ApiResponse({ status: 200, description: 'Ok'})
	async findAllWithOrders(@Param('id') id: string, @Query() query: UserQuerys): Promise<User> {      
    return this.userService.getOrders(id, query || null)
  }
  
  @Get('/:id/signatures')
	@ApiResponse({ status: 200, description: 'Ok'})
	async findAllWithSignature(@Param('id') id: string): Promise<UserSignatureResponse[]> {
    return this.userService.getSignatures(id, ['signatures', 'signatures.plan'])
  }

  @Get('/me/home')
  @UseGuards(JwtAuthGuard)
	@ApiResponse({ status: 200, description: 'Ok'})
	async me(@Req() req): Promise<any> {
    const resp = await this.userService.me(req.user.sub)
    
    return resp
  }
  
  @Get('/export/all')
	@ApiResponse({ status: 200, description: 'Ok'})
	async export(@Res() res): Promise<any> {
    const resp = await this.userService.export()
    
    return res.download(resp)
  }

}
