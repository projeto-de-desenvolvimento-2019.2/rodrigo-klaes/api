import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Module } from '@nestjs/common';
import MailService from './mail.service';

@Module({
    imports: [
         
        MailerModule.forRootAsync({
            useFactory: () => ({
            transport: 'smtps://padocadeliveryapp@gmail.com:jikzrdvjbqooiyti@smtp.gmail.com',
            defaults: {
                from:'"nest-modules" <padocadeliveryapp@gmail.com>',
            },
            template: {
                dir: __dirname + '/templates',
                adapter: new HandlebarsAdapter(),
                options: {
                strict: true,
                },
            },
            }),
        }),
    ],
    providers: [MailService],
    exports: [MailService]
})
export class MailModule {}
