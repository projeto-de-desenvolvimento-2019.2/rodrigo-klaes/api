export interface VerifyCodeParams {
  user_id: string
  code: string
}

export interface NewPasswordParams {
  password: string
  user_id: string
}