export type getAllParams = {
  relations?: string[];
  query?: QueryParams;
};

export class QueryParams {
  search = ''
  page = 0
  size = 10
  getAll = false
}
