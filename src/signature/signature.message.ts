export default {
  NOT_FOUND: {
      message: 'Assinatura não encontrada',
      status: 404
  }
}