const generateRandomCode = (() => {
    const USABLE_CHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789".split("");
  
    return (length: number) => {
      const code = new Array(length).fill(null).map(() => {
        return USABLE_CHARACTERS[Math.floor(Math.random() * USABLE_CHARACTERS.length)];
      }).join("");

      return code
    }
  })();

export default generateRandomCode