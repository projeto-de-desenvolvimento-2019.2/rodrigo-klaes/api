import { Controller, Get, Param, Query } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Order from './entity/order.entity';
import { OrderService } from './order.service';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { QueryParams } from 'src/base/type/getAllParams';

@ApiUseTags('orders')
@Controller('orders')
export class OrderController extends BaseController<Order> {
  constructor(private readonly orderService: OrderService) {
    super(orderService);
  }

  @Get('/products')
  @ApiResponse({ status: 200, description: 'Ok' })
  async findAllWithRelations(@Query() query?: QueryParams): Promise<ResponseType> {
    return await this.orderService.getAll({ relations: ['products'], query });
  }

  @Get('/products/user')
  @ApiResponse({ status: 200, description: 'Ok' })
  async findAllWithRelationsUser(@Query() query?: QueryParams): Promise<ResponseType> {
    return await this.orderService.getAll({
      relations: ['products', 'user', 'schedule', 'schedule.status'],
      query,
    });
  }

  @Get('/:id/products/user')
  @ApiResponse({ status: 200, description: 'Ok' })
  async getWithRelations(@Param('id') id: string): Promise<Order> {
    
    return await this.orderService.get(id, [
      'user',
      'orderProduct',
      'orderProduct.product',
    ]);
  }

  //   @Get('/:id/products/user')
  // 	@ApiResponse({ status: 200, description: 'Ok'})
  // 	async getWithRelationsUser(@Param('id') id: string): Promise<Order> {
  // 		return await this.orderService.get(id, ['products', 'user'])
  // 	}
}
