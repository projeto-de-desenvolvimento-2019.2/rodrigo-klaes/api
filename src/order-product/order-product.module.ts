import { Module } from '@nestjs/common';
import { OrderProductService } from './order-product.service';
import { OrderProductController } from './order-product.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import OrderProduct from './entity/order-product.entity';
import { OrderModule } from '../order/order.module';

@Module({
  imports: [TypeOrmModule.forFeature([OrderProduct]), OrderModule],
  providers: [OrderProductService],
  controllers: [OrderProductController]
})
export class OrderProductModule {}
