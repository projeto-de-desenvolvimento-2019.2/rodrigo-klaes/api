import Order from "src/order/entity/order.entity";

export default class MeResponseType{
  next: Order
  last: Order
  has_feedback: boolean
}