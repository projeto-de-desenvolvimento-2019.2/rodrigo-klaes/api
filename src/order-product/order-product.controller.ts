import { Body, Controller, Delete, Param, Put, UseGuards } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import OrderProduct from './entity/order-product.entity';
import { OrderProductService } from './order-product.service';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
@ApiUseTags('order-products')
@Controller('order-products')
export class OrderProductController extends BaseController<OrderProduct> {
    constructor(private readonly orderProductService: OrderProductService){
        super(orderProductService)
    }

    @Delete('/:id/remove-item')
    @ApiResponse({ status: 200, description: 'Ok' })
    @UseGuards(JwtAuthGuard)
    async removeItem(@Param('id') id: string): Promise<string> {
        
      return await this.orderProductService.removeItem(id); 
    }

    @Put('/:id/update-item/amount/:amount')
    @ApiResponse({ status: 200, description: 'Ok' })
    @UseGuards(JwtAuthGuard)
    async updateItem(@Param('id') id: string, @Param('amount') amount: number): Promise<string> {
        
      return await this.orderProductService.updateItem(id, amount); 
    }
  
}
