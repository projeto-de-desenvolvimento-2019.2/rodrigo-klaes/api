export default {
  NOT_FOUND: {
      message: 'Pedido agendado não encontrado',
      status: 404
  }
}