import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";
import { Column, Entity } from "typeorm";
import BaseEntity from "../../base/base.entity";


@Entity('feedbacks')
export default class Feedback extends BaseEntity {
  @Column()
  @ApiModelProperty()
  readonly note: number

  @Column({ type: 'mediumtext'})
  @ApiModelPropertyOptional()
  readonly comment: string

  @Column()
  @ApiModelProperty()
  readonly order_id: string

  @Column()
  @ApiModelProperty()
  readonly user_id: string
}