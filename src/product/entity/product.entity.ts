import BaseEntity from '../../base/base.entity';
import { Column, Entity, CreateDateColumn, UpdateDateColumn, OneToMany, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import Category from '../../category/entity/category.entity';
import Order from 'src/order/entity/order.entity';
import OrderProduct from 'src/order-product/entity/order-product.entity';

export enum ProductUnity {
    UN = 'unidade',
    G = 'grama',
    KG = 'quilograma',
    LT = 'litro',
    ML = 'mililitro'
}

@Entity('products')
export default class Product extends BaseEntity {
  @Column()
  @ApiModelPropertyOptional()
  readonly name: string;

  @Column()
  @ApiModelPropertyOptional()
  readonly code: string;

  @Column({type: "float"})
  @ApiModelPropertyOptional()
  readonly price: number;

  @Column({ type: "integer"})
  @ApiModelPropertyOptional()
  readonly amount: number;

  @Column({
      type: "enum",
      enum: ProductUnity,
  })
  @ApiModelPropertyOptional()
  readonly unity: ProductUnity;

  @Column()
  @ApiModelPropertyOptional()
  readonly image: string;

  @Column()
  @ApiModelProperty()
  readonly category_id: string;

  @ManyToMany(type => Order)
  @JoinTable({
      name: "order_products",
      joinColumn: {
        name: "product_id",
        referencedColumnName: "id"
      },
      inverseJoinColumn: {
          name: "order_id",
          referencedColumnName: "id"
      }
  })
  readonly orders: Order[]
   
  @ManyToOne(type => Category, category => category.products)
  @JoinColumn({name: 'category_id'})
  readonly category: Category

  @OneToMany(type => OrderProduct, orderProduct => orderProduct.product)
  readonly orderProduct: OrderProduct[]
}