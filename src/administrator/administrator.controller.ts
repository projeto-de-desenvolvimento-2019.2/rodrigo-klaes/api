import { Controller } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Administrator from './entity/administrator.entity';
import { AdministratorService } from './administrator.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('administrators')
@Controller('administrators')
export class AdministratorController extends BaseController<Administrator> {
  constructor(private readonly administratorService: AdministratorService){
    super(administratorService)
  }
}
