import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import Townhouse from './entity/townhouse.entity';
import { TownhouseController } from './townhouse.controller';
import { TownhouseService } from './townhouse.service';

@Module({
  imports: [TypeOrmModule.forFeature([Townhouse])],
  controllers: [TownhouseController],
  providers: [TownhouseService]
})
export class TownhouseModule {}
