import { PrimaryColumn, Column, CreateDateColumn, UpdateDateColumn, Generated } from "typeorm";
import { ApiModelPropertyOptional, ApiModelProperty } from "@nestjs/swagger";

export default class BaseEntity {
    @PrimaryColumn()
    @ApiModelPropertyOptional()
    @Generated("uuid")
    public id: string;
    @Column({default: true})
    @ApiModelPropertyOptional()
    readonly is_active: boolean;
    @CreateDateColumn({type: 'timestamp'})
    readonly created_at: Date;
    @UpdateDateColumn({type: 'timestamp'})
    readonly updated_at: Date;
}