import { Controller } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Signature from './entity/signature.entity';
import { SignatureService } from './signature.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('signatures')
@Controller('signatures')
export class SignatureController extends BaseController<Signature> {
  constructor(private readonly signatureService: SignatureService){
    super(signatureService)
  }
}


