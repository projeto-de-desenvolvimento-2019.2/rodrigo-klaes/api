import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import User from './entity/user.entity';
import { ScheduleModule } from 'src/schedule/schedule.module';
import { OrderModule } from 'src/order/order.module';
import { FeedbackModule } from 'src/feedback/feedback.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), ScheduleModule, OrderModule, FeedbackModule],
  providers: [UserService],
  controllers: [UserController],
  exports: [UserService]
})
export class UserModule {}
